﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace VIConnectionStrings
{
    public static class Utils
    {
        private static List<ConnectionString> Connections { get; set; }

        private static string FILE_PATH_BACKOFFICE = @"C:\VIBO\Conn\";
        private static string AUTH_FILE_BACKOFFICE = @"C:\VIBO\Conn.txt";

        public static string GetPassword(string name, string filePath = null, string source = null, string ip = null)
        {
            if (String.IsNullOrEmpty(name))
                return null;

            if (String.IsNullOrEmpty(filePath))
                filePath = FILE_PATH_BACKOFFICE;

            try
            {
                name = name.ToUpper();

                if (Connections == null)
                    Connections = new List<ConnectionString>();

                ConnectionString connection = Connections.Find(c => c.Name == name);

                if (connection != null)
                    return connection.Password;

                string encryptedPassword = null;

                try
                {
                    encryptedPassword = System.IO.File.ReadAllText(filePath + name + ".txt");
                }
                catch (Exception ex)
                {
                    SendErrorEmail("Failed to get encrypted password for (" + name + ", " + filePath + ", " + source + ", " + ip + "): " + ex, "ConnectionStrings.cs");
                }

                if (!String.IsNullOrEmpty(encryptedPassword))
                {
                    string decryptedPassword = Decrypt(encryptedPassword);
                    Connections.Add(new ConnectionString()
                    {
                        Name = name.ToUpper(),
                        Password = decryptedPassword
                    });

                    return decryptedPassword;
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("Exception getting connection string password (" + name + ", " + filePath + ", " + source + ", " + ip + "): " + ex, "ConnectionStrings.cs");
            }

            SendErrorEmail("Encrypted password not found for (" + name + ", " + filePath + ", " + source + ", " + ip + ")", "ConnectionStrings.cs");
            return null;
        }

        public static bool SetPassword(string auth, string name, string password, string authFile = null, string filePath = null)
        {
            if (String.IsNullOrEmpty(auth) || String.IsNullOrEmpty(name))
                return false;

            if (String.IsNullOrEmpty(filePath))
                filePath = FILE_PATH_BACKOFFICE;

            if (String.IsNullOrEmpty(authFile))
                authFile = AUTH_FILE_BACKOFFICE;

            if (!DevOne.Security.Cryptography.BCrypt.BCryptHelper.CheckPassword(auth, System.IO.File.ReadAllText(authFile)))
                return false;

            name = name.ToUpper();
            string file = filePath + name + ".txt";

            System.IO.Directory.CreateDirectory(filePath);

            bool success = false;

            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(file, false))
            {
                try
                {
                    writer.Write(Encrypt(password));
                    success = true;
                }
                catch (Exception ex)
                {
                    SendEmail("error@voipinnovations.com", "jasonm@voipinnovations.com", "VIConnectionStrings.dll SetPassword exception", "SetPassword(" + auth + ", " + name + ", " + password + ", " + authFile + ", " + filePath + "): " + ex);
                }
            }

            return success;
        }

        private static byte[] key = Encoding.UTF8.GetBytes("M9VHuHpK"); //new byte[8] { 1, 2, 3, 4, 5, 6, 7, 8 };
        private static byte[] iv = Encoding.UTF8.GetBytes("e5Ou/7AlTXY5IjtIneNv5uAzRPmpQH4VdWnG1vGpy.nSLMGbCTYnC");

        public static string Encrypt(string text)
        {
            SymmetricAlgorithm algorithm = DES.Create();
            ICryptoTransform transform = algorithm.CreateEncryptor(key, iv);
            byte[] inputbuffer = Encoding.Unicode.GetBytes(text);
            byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);
            return Convert.ToBase64String(outputBuffer);
        }

        public static string Decrypt(string text)
        {
            SymmetricAlgorithm algorithm = DES.Create();
            ICryptoTransform transform = algorithm.CreateDecryptor(key, iv);
            byte[] inputbuffer = Convert.FromBase64String(text);
            byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);
            return Encoding.Unicode.GetString(outputBuffer);
        }

        public static void SendErrorEmail(string message, string source, string recipient = "jasonm@voipinnovations.com")
        {
            string subject = "Back Office Error - " + source;
            SendEmail("error@voipinnovations.com", recipient, subject, message);
        }

        public static bool SendEmail(string from, string recipient, string subject, string message, string bcc = null, List<string> attachments = null, bool isHTML  = true, string cc = null)
        {
            try
            {
                using (MailMessage msg = new MailMessage())
                {
                    if (from == null)
                        msg.From = new MailAddress("DoNotReply@voipinnovations.com", "VoIP Innovations Notification Mailer - DO NOT REPLY");
                    else
                        msg.From = new MailAddress(from);

                    if (!String.IsNullOrEmpty(recipient))
                        msg.To.Add(recipient);
                    msg.Subject = subject;
                    msg.IsBodyHtml = isHTML;
                    msg.Body = message;
                    if (!String.IsNullOrEmpty(bcc))
                    {
                        if (bcc.Contains(","))
                        {
                            string[] bccs = bcc.Split(',');
                            if (bccs != null)
                            {
                                foreach (string bccEmail in bccs)
                                {
                                    //if (isEmail(bccEmail))
                                    msg.Bcc.Add(bccEmail);
                                }
                            }
                        }
                        else if (bcc.Contains(";"))
                        {
                            string[] bccs = bcc.Split(';');
                            if (bccs != null)
                            {
                                foreach (string bccEmail in bccs)
                                {
                                    //if (isEmail(bccEmail))
                                    msg.Bcc.Add(bccEmail);
                                }
                            }
                        }
                        else //if (isEmail(bcc))
                            msg.Bcc.Add(bcc);
                    }

                    if (!String.IsNullOrEmpty(cc))
                        msg.CC.Add(cc);

                    if (attachments != null && attachments.Count > 0)
                    {
                        foreach (string attachment in attachments)
                            msg.Attachments.Add(new Attachment(attachment));
                    }

                    using (SmtpClient smtp = new SmtpClient("MTA01.INSIDE.ADBASESYSTEMS.COM"))
                    {
                        smtp.Send(msg);
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                //LOGG.Error("SendEmail exception using MTA01, trying MTA02 (" + from + ", to " + recipient + ", subject: " + subject + ", message: " + message +
                //    ", bcc: " + bcc + ", attchment: " + (attachments == null ? "false" : "true") + ") " + Environment.NewLine + ex.Message);

                try
                {
                    using (MailMessage msgMTA02 = new MailMessage())
                    {
                        if (from == null)
                            msgMTA02.From = new MailAddress("DoNotReply@voipinnovations.com", "VoIP Innovations Notification Mailer - DO NOT REPLY");
                        else
                            msgMTA02.From = new MailAddress(from);

                        msgMTA02.To.Add(recipient);
                        msgMTA02.Subject = subject;
                        msgMTA02.IsBodyHtml = isHTML;
                        msgMTA02.Body = message;
                        if (!String.IsNullOrEmpty(bcc))
                            msgMTA02.Bcc.Add(bcc);

                        if (!String.IsNullOrEmpty(cc) /*&& isEmail(cc)*/)
                            msgMTA02.CC.Add(cc);

                        if (attachments != null && attachments.Count > 0)
                        {
                            foreach (string attachment in attachments)
                                msgMTA02.Attachments.Add(new Attachment(attachment));
                        }

                        using (SmtpClient smtp = new SmtpClient("MTA02.INSIDE.ADBASESYSTEMS.COM"))
                        {
                            smtp.Send(msgMTA02);
                        }

                        return true;
                    }
                }
                catch (Exception ex2)
                {
                    //LOGG.Error("SendEmail exception USING MTA02 (" + from + ", to " + recipient + ", subject: " + subject + ", message: " + message +
                    //", bcc: " + bcc + ", attchment: " + (attachments == null ? "false" : "true") + ") " + Environment.NewLine + ex2.Message);
                }
            }

            return false;
        }
    }

    public class ConnectionString
    {
        public string Name { get; set; }
        public string Password { get; set; }

        public ConnectionString() { }
    }

}
